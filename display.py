

import pygame
from multiprocessing import Process, Queue
from pygame.locals import *


from multiprocessing import Process, Queue

RED = (0xFF, 0x00, 0x00)
BLUE = (0X00, 0x00, 0xFF)
GREEN = (0x00, 0xFF, 0x00)
BLACK = (0x00, 0x00, 0x00)

DRAW_GRID = False
DEBUG_KEYS = False

UI_STATE_NORMAL = 0
UI_STATE_TIMEENTRY = 1
UI_STATE_STOPENTRY = 2
UI_STATE_TESTSTRIPRUN = 10
UI_STATE_TESTSTRIPENTRY_OUTERSTEP = 11
UI_STATE_TESTSTRIPENTRY_NOSTRIPS = 12
UI_STATE_TESTSTRIPRUNPAUSE = 13
UI_STATE_RUNNING = 20
UI_STATE_RUN_HOLDING = 21
UI_STATE_FOCUSING = 30
UI_STATE_FOCUSING_HOLDING = 31



#SCREEN_SIZE = (1280, 960)
FULLSCREEN = False
SCREEN_SIZE = (1024, 768)
LIT_CIRCLE_WIDTH = 100
LIT_CIRCLE_POS = (SCREEN_SIZE[0] - LIT_CIRCLE_WIDTH - 20, SCREEN_SIZE[1] - LIT_CIRCLE_WIDTH - 20)



class Display():
    def __init__(self):
        pygame.init()
        pygame.display.set_caption("Darkroomtimer - DRT")

        self.quitting = False
        #screen = pygame.display.set_mode((640, 480))
        if FULLSCREEN:
            self.screen = pygame.display.set_mode(SCREEN_SIZE, pygame.FULLSCREEN)
        else:
            self.screen = pygame.display.set_mode(SCREEN_SIZE)
        
        self.labelFont = pygame.font.SysFont("monospace", 16)
        self.timerFont = pygame.font.SysFont("monospace", 50)
        self.runningTimerFont = pygame.font.SysFont("monospace", 100)
        self.gridFont = pygame.font.SysFont("monospace", 10)
        self.state = UI_STATE_NORMAL
        self.numericBuffer = ""
        self.baseTime = 16.0
        self.stops = 0.0
        self.testStripOuterStep = 2.0
        self.testStripNoStrips = 4
        self.testStripCurrentStep = 0
        self.lightOnCallback = None
        self.lightOffCallback = None
        self.offMillis = 0
        self.lightsLit = False

    def lightOn(self):
        print "Switching light on"
        self.lightsLit = True
        if self.lightOnCallback:
            self.lightOnCallback()
        else:
            print "Light on callback not set"

    def lightOff(self):
        print "Switching light off"
        self.lightsLit = False
        self.offMillis = 0
        if self.lightOffCallback:
            self.lightOffCallback()
        else:
            print "Light off callback not set"

    def getRealExposure(self):
        return self.calculateExposure(self.baseTime, self.stops)

    def calculateExposure(self, time, stops):
        return time * 2 ** stops

    def createLabel(self, text, font):
        return font.render(text, 1, RED, BLACK)

    def drawLabel(self, text, font, x, y):
        label = self.createLabel(str(text), font)
        self.screen.blit(label, (x, y))

    def getTestStripSteps(self):
        rv = []
        for x in range(1, self.testStripNoStrips + 1):
            rv.append(0 - (self.testStripOuterStep / self.testStripNoStrips * x))

        rv = rv[::-1]
        rv.append(0)

        for x in range(1, self.testStripNoStrips + 1):
            rv.append((self.testStripOuterStep / self.testStripNoStrips * x))

        return rv

    def handleEvent(self, event):
        try:
            if event.type == KEYDOWN:
                mods = pygame.key.get_mods() 
                if DEBUG_KEYS:
                    print "{0}: {1}".format(mods, event.key)
                if not (mods & (KMOD_CTRL | KMOD_ALT | KMOD_SHIFT)):
                    if event.key in [K_ESCAPE]:
                        self.state = UI_STATE_NORMAL
                        self.lightOff()
                    elif event.key in [K_c, K_z]:
                        self.state = UI_STATE_NORMAL
                    elif event.key in [K_b]:
                        self.state = UI_STATE_TIMEENTRY
                        self.numericBuffer = ""
                    elif event.key in [K_t]:
                        self.state = UI_STATE_TESTSTRIPENTRY_OUTERSTEP
                        self.bufferValue = str(self.testStripOuterStep)
                    elif event.key in [K_s]:
                        self.state = UI_STATE_STOPENTRY
                        self.numericBuffer = ""                        
                    elif event.key >= K_KP0 and event.key <= K_KP9:
                        self.handleNumeric(event.key - K_KP0)
                    elif event.key >= K_0 and event.key <= K_9:
                        self.handleNumeric(event.key - K_0)
                    elif event.key == K_BACKSPACE:
                        self.handleNumeric("B")
                    elif event.key in [K_COMMA, K_PERIOD, K_KP_PERIOD]:
                        self.handleNumeric(".")
                    elif event.key in [K_n, K_KP_MINUS, K_MINUS]:
                        self.negateBuffer()
                    elif event.key in [K_RETURN, K_KP_ENTER]:
                        self.handleEnter()
                    elif event.key in [K_UP]:
                        self.stops += 0.25
                    elif event.key in [K_DOWN]:
                        self.stops -= 0.25
                    elif event.key in [K_SPACE]:
                        self.handleSpace()

                elif mods & pygame.KMOD_CTRL:
                    if event.key in [K_c]:
                        print "Quitting"
                        self.quitting = True
                    elif event.key in [K_RETURN, K_KP_ENTER]:
                        print("Resetting base time")
                        self.baseTime = self.getRealExposure()
                        self.stops = 0.0
                    elif event.key in [K_t]:
                        self.state = UI_STATE_TESTSTRIPRUNPAUSE
                        self.testStripCurrentStep = 0     
                    elif event.key in [K_r]:
                        self.state = UI_STATE_RUN_HOLDING
                        self.testStripCurrentStep = 0  
                    elif event.key in [K_l]:
                        self.state = UI_STATE_FOCUSING_HOLDING
                elif mods & pygame.KMOD_SHIFT:
                    if event.key in [K_b]:
                        self.state = UI_STATE_TIMEENTRY
                        self.numericBuffer = str(self.baseTime)
                    elif event.key in [K_s]:
                        self.state = UI_STATE_STOPENTRY
                        self.numericBuffer = str(self.stops)
                    elif event.key in [K_UP]:
                        self.stops += 1
                    elif event.key in [K_DOWN]:
                        self.stops -= 1

        except Exception as ex:
            print ex

    def handleSpace(self):
        if self.state in [UI_STATE_TESTSTRIPRUNPAUSE]:
            exposures = self.getTestStripSteps()
            ourTime = self.calculateExposure(self.baseTime, exposures[self.testStripCurrentStep])
            print "Calculating exposure for step: {0}, with time {1}".format(self.testStripCurrentStep, ourTime)
            if self.testStripCurrentStep > 0:
                removeTime = self.calculateExposure(self.baseTime, exposures[self.testStripCurrentStep - 1])
                print "Removing already performed time {0}".format(removeTime)
                ourTime -= removeTime
            self.state = UI_STATE_TESTSTRIPRUN
            self.setOffTime(ourTime)
        elif self.state in [UI_STATE_RUN_HOLDING]:
            self.state = UI_STATE_RUNNING
            self.setOffTime(self.getRealExposure())
        elif self.state in [UI_STATE_FOCUSING_HOLDING]:
            self.state = UI_STATE_FOCUSING
            self.lightOn()
        elif self.state in [UI_STATE_FOCUSING]:
            self.state = UI_STATE_FOCUSING_HOLDING
            self.lightOff()

    def setOffTime(self, inSeconds):
        now = pygame.time.get_ticks()
        print inSeconds
        self.offMillis = now + inSeconds * 1000
        print now
        print self.offMillis
        self.lightOn()

    def handleTimer(self):
        now = pygame.time.get_ticks()
        if self.state in [UI_STATE_TESTSTRIPRUN, UI_STATE_RUNNING]:
            if self.offMillis <= now:
                self.lightOff()
                if self.state in [UI_STATE_RUNNING]:
                    self.state = UI_STATE_NORMAL
                elif self.state in [UI_STATE_TESTSTRIPRUN]:
                    self.state = UI_STATE_TESTSTRIPRUNPAUSE
                    self.testStripCurrentStep += 1
                    if self.testStripCurrentStep >= (self.testStripNoStrips * 2) + 1:
                        self.testStripCurrentStep = 0
                        self.state = UI_STATE_NORMAL

    def negateBuffer(self):
        if self.state in [UI_STATE_STOPENTRY]:
            if self.numericBuffer[0] == "-":
                self.numericBuffer = self.numericBuffer[1:]
            else:
                self.numericBuffer = "-" + self.numericBuffer

    def handleNumeric(self, number):
        newValue = str(number)
        if newValue == ".":
            if not self.state in [UI_STATE_TESTSTRIPENTRY_NOSTRIPS]:
                self.numericBuffer = self.numericBuffer.replace(".", "")
                self.numericBuffer += "."
        elif newValue == "B":
            self.numericBuffer = self.numericBuffer[:-1]
        else:
            self.numericBuffer += newValue

    def handleEnter(self):
        if self.state == UI_STATE_TIMEENTRY:
            self.baseTime = self.popBufferAsFloat()
        elif self.state == UI_STATE_STOPENTRY:
            self.stops = self.popBufferAsFloat()
        elif self.state == UI_STATE_TESTSTRIPENTRY_OUTERSTEP:
            self.testStripOuterStep = self.popBufferAsFloat()
            self.bufferValue = str(self.testStripNoStrips)
            self.state = UI_STATE_TESTSTRIPENTRY_NOSTRIPS
        elif self.state == UI_STATE_TESTSTRIPENTRY_NOSTRIPS:
            self.testStripNoStrips = self.popBufferAsInt()
            self.state = UI_STATE_NORMAL

            

    def popBufferAsFloat(self):
        bufferValue = float(self.numericBuffer)
        self.numericBuffer = ""
        return bufferValue

    def popBufferAsInt(self):
        bufferValue = int(self.numericBuffer)
        self.numericBuffer = ""
        return bufferValue

    def getStateName(self):
        if self.state == UI_STATE_NORMAL:
            return "Normal"
        elif self.state == UI_STATE_TIMEENTRY:
            return "Time entry"
        elif self.state == UI_STATE_STOPENTRY:
            return "Stops entry"
        elif self.state == UI_STATE_TESTSTRIPRUN:
            return "Running test strip"
        elif self.state == UI_STATE_TESTSTRIPRUNPAUSE:
            return "Test strip mode"
        elif self.state == UI_STATE_TESTSTRIPENTRY_OUTERSTEP:
            return "Outer Max step entry"
        elif self.state == UI_STATE_TESTSTRIPENTRY_NOSTRIPS:
            return "Strip count entry"
        elif self.state == UI_STATE_RUN_HOLDING:
            return "Run mode, waiting"
        elif self.state == UI_STATE_FOCUSING:
            return "Focusing"
        elif self.state == UI_STATE_FOCUSING_HOLDING:
            return "Focusing, waiting"
        else:
            return self.state

    def tick(self):
        #print "Drawing"
        now = pygame.time.get_ticks()
        self.handleTimer()
        pygame.draw.rect(self.screen, (0, 0, 0), Rect((0, 0), SCREEN_SIZE))
        if DRAW_GRID:
            for x in range(0, 100):
                self.drawLabel(str(x * 4), self.gridFont, x * 40 + 5, 2)
                pygame.draw.line(self.screen, BLUE, [x * 40, 0], [x * 40, 2000], 2)
            for y in range(0, 100):
                self.drawLabel(str(y * 4), self.gridFont, 2, y * 40 + 5)
                pygame.draw.line(self.screen, GREEN, [0, y * 40], [2000, y * 40], 2)

        self.drawLabel("Base time: {0:.3f}".format(self.baseTime), self.timerFont, 20, 10)
        self.drawLabel("Stops adj: {0:.2}".format(self.stops), self.timerFont, 20, 60)
        self.drawLabel("Real time: {0:.3f}".format(self.getRealExposure()), self.timerFont, 20, 110)

        self.drawLabel("Test strip {0} step, with {1} strips".format(self.testStripOuterStep, self.testStripNoStrips * 2 + 1 ), self.labelFont, 600, 40)
        testStripSteps = self.getTestStripSteps()
        lastStrip = 0
        for x in range(0, len(testStripSteps)):
            steps = testStripSteps[x]
            exposure = self.calculateExposure(self.baseTime, steps)
            label = "{0}: {1:.2f} step = {2:.3f}s, real = {3:.3f}s".format(x, steps, exposure, exposure - lastStrip)
            if self.state in [UI_STATE_TESTSTRIPRUN, UI_STATE_TESTSTRIPRUNPAUSE] and self.testStripCurrentStep == x:
                label = "-->" + label
            self.drawLabel(label, self.labelFont, 600, 80 + 20 * x)
            lastStrip = exposure

        self.drawLabel("State: {0}".format(self.getStateName()), self.labelFont, 20, 200)
        self.drawLabel("Buffer: {0}".format(self.numericBuffer), self.labelFont, 20, 220)

        if self.offMillis > now:
            remainingTime = (self.offMillis - now) / 1000

            self.drawLabel("{0:.2f}s".format(remainingTime), self.runningTimerFont, 20, 320)



        if self.state in [UI_STATE_TESTSTRIPRUNPAUSE, UI_STATE_TESTSTRIPRUN, UI_STATE_FOCUSING,UI_STATE_FOCUSING_HOLDING, UI_STATE_RUNNING, UI_STATE_RUN_HOLDING]:
            pygame.draw.circle(self.screen, RED, LIT_CIRCLE_POS, LIT_CIRCLE_WIDTH, 10)

        if self.lightsLit:
            pygame.draw.circle(self.screen, RED, LIT_CIRCLE_POS, LIT_CIRCLE_WIDTH)

        for event in pygame.event.get():
            self.handleEvent(event)

        pygame.display.flip()
